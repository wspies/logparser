package com.ef.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "accesslog",indexes = { @Index(name = "idx_date", columnList = "date")})
public class AccessLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    @Column(name = "date")
    private Date date;

    @Column(name = "ip", length = 15)
    private String ip;

    @Column(name = "request",length = 15)
    private String request;

    @Column(name = "status", length = 3)
    private String status;

    @Column(name = "useragent")
    private String useragent;

    @Override
    public String toString() {
        return "AccessLog{" +
                "id=" + id +
                ", date=" + date +
                ", ip='" + ip + '\'' +
                ", request_type='" + request + '\'' +
                ", status='" + status + '\'' +
                ", useragent='" + useragent + '\'' +
                '}';
    }
}
