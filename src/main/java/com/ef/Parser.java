package com.ef;

import com.ef.services.AccessLogService;
import com.ef.utils.ArgumentParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.SQLException;

@SpringBootApplication
public class Parser implements CommandLineRunner {

    @Autowired
    AccessLogService accessLogService;

    public static void main(String[] args) {

        SpringApplication.run(Parser.class, args);


    }

    @Override
    public void run(String... args) throws Exception {

        /*
        *    - Parse the arguments from the commandline
        *    - check if all mandatory arguments are available
        *    - parse the values into the correct types
        **/

        ArgumentParser argumentParser = new ArgumentParser(args);
        argumentParser.parse();

        accessLogService.setAccessLogLocation(argumentParser.getAccessLog());
        accessLogService.setStartDate(argumentParser.getStartDate());
        accessLogService.setDuration(argumentParser.getDuration());
        accessLogService.setThreshold(argumentParser.getThreshold());

        accessLogService.saveDataIntoDatabase();
        accessLogService.processIpAddressesToBlock();
        accessLogService.displayBlockedAddresses();

    }
}
