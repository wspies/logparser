package com.ef.services;

import com.ef.models.AccessLog;
import com.ef.models.BlockedIp;
import com.ef.repositories.AccessLogRepository;
import com.ef.repositories.BlockedIpRepository;
import com.ef.utils.DateFormatter;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
@Service
public class AccessLogServiceImpl implements AccessLogService{

    private final String DATE_FORMAT_DB = "yyyy-MM-dd HH:mm:ss";
    private String reason = "%d or more request per %s starting at %s";

    private AccessLogRepository accessLogRepository;
    private BlockedIpRepository blockedIpRepository;
    private List<BlockedIp> blockedIpList;
    private String accessLogLocation;
    private Integer threshold;
    private String startDate;
    private String duration;

    public AccessLogServiceImpl(AccessLogRepository accessLogRepository,BlockedIpRepository blockedIpRepository) {
        this.accessLogRepository = accessLogRepository;
        this.blockedIpRepository = blockedIpRepository;
    }

    @Override
    public void save(AccessLog accessLog) {

        this.accessLogRepository.save(accessLog);

    }

    /*
    * Various steps to make the access log is
    * loaded into the database
    * 1. delete old records
    * 2. make sure the database allows us to use LOAD DATA
    * 3. save the access log
    * */
    @Override
    public void saveDataIntoDatabase() {

        this.accessLogRepository.deleteAll();

        /* Assuming here we have the rights to set Global vars in Mysql*/
        this.accessLogRepository.setGlobalLocalInFile();

        /* Fastest loading of the accesslog file is via LOAD DATA LOCAL INFILE*/
        this.accessLogRepository.saveAccessLog(this.getAccessLogLocation());
    }
    /*
    * Various steps to find ip address
    * matching the arguments supplied on the
    * commandline
    * 1. execute query to retrieve ip addresses
    * 2. format and add the blocking reason
    * 3. save the result to the databse
    * */
    @Override
    public void processIpAddressesToBlock(){

        findRequestMatchingArgs();

        updateBlockingReason();

        saveBlockedIpAddresses();

    }


    private void updateBlockingReason() {
        String reason = formatBlockingReason();
        this.blockedIpList.forEach(c -> c.setReason(reason));
    }

    private void saveBlockedIpAddresses() {

        blockedIpRepository.saveAll(this.blockedIpList);
    }

    private void findRequestMatchingArgs() {
        this.blockedIpList = this.accessLogRepository.findMatchingRequests(this.getStartDate(),this.getDuration(),this.threshold);
    }

    @Override
    public void displayBlockedAddresses() {
        blockedIpRepository.findAll().forEach(c -> System.out.println(c.toString()));
    }

    private String formatBlockingReason(){
        return String.format(reason, this.getThreshold(), this.getDuration().toLowerCase(),this.getStartDate());
    }

    public void setAccessLogLocation(String accessLogLocation) {
        this.accessLogLocation = accessLogLocation;
    }

    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }

    public void setStartDate(Date startDate) {

        DateFormatter formatter = new DateFormatter(DATE_FORMAT_DB);
        this.startDate = formatter.format(startDate);
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
