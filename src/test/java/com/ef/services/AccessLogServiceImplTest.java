package com.ef.services;

import com.ef.models.AccessLog;
import com.ef.repositories.AccessLogRepository;
import com.ef.repositories.BlockedIpRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class AccessLogServiceImplTest {

    AccessLogService accessLogService;

    @Mock
    AccessLogRepository accessLogRepository;

    @Mock
    BlockedIpRepository blockedIpRepository;

    @Before
    public void setup() {

        MockitoAnnotations.initMocks(this);
        accessLogService = new AccessLogServiceImpl(accessLogRepository,blockedIpRepository);

    }

    @Test
    public void WhenFindAll_ThenReturn1(){

        //Given
        AccessLog accessLog = new AccessLog();

        try{
            Date accessLogDate = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss").parse("2017-01-01 00:00:11.763");
            accessLog.setDate(accessLogDate);
        }
        catch (ParseException e){}

        accessLog.setIp("192.168.1.1");
        accessLog.setRequest("GET / HTTP/1.1");
        accessLog.setStatus("200");
        accessLog.setUseragent("swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0");

        List<AccessLog> accessLogList = new ArrayList<>();
        accessLogList.add(accessLog);

        when(accessLogRepository.findAll()).thenReturn(accessLogList);

        //when
        List<AccessLog> accessLogs = accessLogRepository.findAll();

        //then
        assertEquals(1,accessLogs.size());



    }


}