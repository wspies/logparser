package com.ef.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "blockedip")
public class BlockedIp {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;

    @Column(name = "ip",length = 15)
    private String ip;

    @Column(name = "reason")
    private String reason;

    @Override
    public String toString() {
        return ip + " " + reason;
    }
}