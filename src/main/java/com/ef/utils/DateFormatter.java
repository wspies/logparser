package com.ef.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatter {

    private String format;
    private SimpleDateFormat simpleDateFormat;

    public DateFormatter(String format) {
        this.format = format;
        simpleDateFormat = new SimpleDateFormat(format);
    }

    public Date parse(String date) throws ParseException {
        return simpleDateFormat.parse(date);
    }

    public String format(Date date){
        return simpleDateFormat.format(date);
    }


}
