package com.ef.repositories;

import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface AccessLogRepositoryCustom{

    public void saveAccessLog(@Param("file")String file);
    public void setGlobalLocalInFile();
    List findMatchingRequests(String startDate, String duration, Integer threshold);
}
