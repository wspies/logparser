package com.ef.services;

import com.ef.models.AccessLog;
import com.ef.models.BlockedIp;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface AccessLogService {

    void save(AccessLog accessLog);
    void saveDataIntoDatabase();
    void displayBlockedAddresses();
    void setStartDate(Date startDate);
    void setAccessLogLocation(String accessLogLocation);
    void setDuration(String duration);
    void setThreshold(Integer thresHold);
    void processIpAddressesToBlock();
    String getAccessLogLocation();




}
