package com.ef.models;

public enum Duration {

    HOURLY("HOUR"),
    DAILY("DAY");

    private final String value;

    Duration(String duration) {
        this.value = duration;
    }

    public String getValue() {
        return value;
    }
}
