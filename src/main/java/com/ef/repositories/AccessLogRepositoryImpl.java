package com.ef.repositories;

import com.ef.models.AccessLog;
import com.ef.models.BlockedIp;
import com.ef.models.Duration;
import com.ef.utils.DateFormatter;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.query.Param;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AccessLogRepositoryImpl implements AccessLogRepositoryCustom{

    @PersistenceContext
    EntityManager entityManager;

    /*
    * Custom Repository implementation to
    * load the csv file into the database
    * using LOAD DATA LOCAL. This is the
    * fastest and most efficient way of
    * importing bulk data into MySql
    * */
    @Override
    @Transactional
    @Modifying
    public void saveAccessLog(String accessLogLocation){

        String sql = "LOAD DATA LOCAL INFILE :fileLocation ";
        sql +=  "INTO TABLE accesslog ";
        sql +=  "FIELDS ";
        sql +=  "   TERMINATED BY '|' ";
        sql +=  "   ENCLOSED BY '\"' ";
        sql +=  "LINES ";
        sql +=  "   TERMINATED BY '\\r\\n' ";
        sql +=  "(date,ip,request,STATUS,useragent)\t\n";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("fileLocation",accessLogLocation);

        query.executeUpdate();

    };


    /*
    * When using LOAD DATA in MySql
    * the Global Local_infile should
    * be set to ON
    * */
    @Override
    @Transactional
    @Modifying
    public void setGlobalLocalInFile(){

        String sql = "SET GLOBAL local_infile = 'ON'";
        Query query = entityManager.createNativeQuery(sql);

        query.executeUpdate();
    };

    @Override
    @Transactional
    @Modifying
    public List<BlockedIp> findMatchingRequests(String startDate, String duration, Integer threshold) {

        String sql = "SELECT id, ip, '' AS reason, COUNT(ip) AS cnt FROM accesslog ";
        sql += "WHERE DATE BETWEEN :startDate AND DATE_ADD(:startDate,INTERVAL 1 " + duration +") ";
        sql += "GROUP BY ip HAVING cnt >= :threshold";

        Query query = entityManager.createNativeQuery(sql,BlockedIp.class);

        query.setParameter("startDate",startDate);
        query.setParameter("threshold",threshold);

        List <BlockedIp> blockedIpList  = query.getResultList();

        return blockedIpList;
    }

}
