package com.ef.repositories;

import com.ef.models.AccessLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface AccessLogRepository extends JpaRepository<AccessLog, Long>, AccessLogRepositoryCustom {
}
