package com.ef.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class ArgumentParserTest {

    @Test
    public void whenMissingArgument_thenException() {
        //Given
        String[] args = {"-threshold", "arg1", "-duration", "arg2"};
        ArgumentParser argumentParser = new ArgumentParser(args);

        //When
        try {
            argumentParser.parse();
            fail("no exception..");
        } catch (ArgumentParserException e) {

            //Then
            // We expect an exception, ending up in the catch makes sense
            assertEquals(true, true);
        }
    }

    @Test
    public void whenThresHoldArgumentWithWrongType_thenException() {
        //Given
        String[] args = {"-threshold", "test", "-duration", "daily","-startDate","017-01-01.15:00:00"};
        ArgumentParser argumentParser = new ArgumentParser(args);

        //When
        try {
            argumentParser.parse();
            System.out.println(argumentParser.getThreshold());
            fail("no exception..");
        } catch (ArgumentParserException e) {

            //Then
            // We expect an exception, ending up in the catch makes sense
            assertEquals(true, true);
        }
    }
}