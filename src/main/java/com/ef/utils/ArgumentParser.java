package com.ef.utils;

import com.ef.models.Duration;
import org.apache.commons.cli.*;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class ArgumentParser {

    private String[] args;
    private Options options = new Options();
    private String accesslog = "access.log";
    private Integer threshold;
    private Date startDate;
    private String duration;

    private final static String DATE_FORMAT_CLI = "yyyy-MM-dd.HH:mm:ss";

    private final static String ERROR_FILE_DOES_NOT_EXIST = "Accesslog cannot be found";
    private final static String ERROR_INVALID_DURATION = "duration can only be daily or hourly";

    public ArgumentParser(String[] args) {
        this.args = args;

        Option accesslog = Option.builder("acceslog")
                .argName("accesslog")
                .longOpt("accesslog")
                .hasArg()
                .desc("path to the web server access log")
                .build();

        Option startDate = Option.builder("startDate")
                .argName("startDate")
                .required()
                .longOpt("startDate")
                .hasArg()
                .desc("start date in yyyy-MM-dd.HH:mm:ss format")
                .build();

        Option duration = Option.builder("duration")
                .argName("duration")
                .longOpt("duration")
                .required()
                .hasArg()
                .desc("daily or hourly")
                .build();

        Option threshold = Option.builder("threshold")
                .argName("threshold")
                .required()
                .longOpt("threshold")
                .hasArg()
                .desc("number of requests")
                .build();

        options.addOption(accesslog);
        options.addOption(startDate);
        options.addOption(duration);
        options.addOption(threshold);
    }

    public void parse() {

        try{
            CommandLineParser commandlineparser = new DefaultParser();
            CommandLine commandline = commandlineparser.parse(options,args,false);

            this.setAccesslog(commandline.getOptionValue("accesslog"));
            this.setStartDate(commandline.getOptionValue("startDate"));
            this.setThreshold(commandline.getOptionValue("threshold"));
            this.setDuration(commandline.getOptionValue("duration"));
        }
        catch(Exception e){

            throw new ArgumentParserException(e.getMessage());
        }

    }

    private void help(){

        HelpFormatter helpFormatter = new HelpFormatter();
        helpFormatter.printHelp("webserver log parser",options);
    }

    public void setAccesslog(String accesslog) {

        if(accesslog == null)
            accesslog = this.accesslog;

        checkFileExists(accesslog);
        this.accesslog = accesslog;
    }

    private void checkFileExists(String accesslog){

        File accessLogFile = new File(accesslog);

        if(!accessLogFile.exists())
            throw new ArgumentParserException(ERROR_FILE_DOES_NOT_EXIST);
    }

    public void setThreshold(String threshold) {

        try{
            this.threshold = Integer.valueOf(threshold);
        }
        catch (Exception e){
            throw new ArgumentParserException(e.getMessage());
        }
    }

    private Date formatDate(String startDate){

        Date date;
        DateFormatter dateFormatter = new DateFormatter(DATE_FORMAT_CLI);

        try {
            date = dateFormatter.parse(startDate);
        }
        catch(ParseException e){
            throw new ArgumentParserException(e.getMessage());
        }

        return date;
    }

    public void setStartDate(String startDate) {

        this.startDate = formatDate(startDate);
    }

    public void setDuration(String duration) {

       try{
           this.duration = Duration.valueOf(duration.toUpperCase()).getValue();
       }
        catch(Exception e){
           throw new ArgumentParserException(ERROR_INVALID_DURATION);
        }
    }

    public String getAccessLog() {
        return accesslog;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getDuration() {
        return duration;
    }
}

