# Log parser

The goal of this project is to write a parser in Java that parses web server access log file, loads the log to MySQL and checks if a given IP makes more than a certain number of requests for the given duration. 

The following deliverable can be found in the deliverables folder:

1 - jar file to be run from the commandline (parser.jar)  
2 -	MySQL schema used for the log data (schema-reation)  
3 - SQL queries for SQL test (queries.sql)  
     
Run the jar:
java -jar parser.jar --accesslog=c:\temp\access.log --startDate=2017-01-01.00:00:00 --duration=daily --threshold=200
 
-- startDate needs to be in yyyy-MM-dd.HH:mm:ss format   
-- accesslog can be left out, the jar will try to locate an access.log file in the same directory the jar file is located  
-- duration can only be daily or hourly  
-- threshold can be any number  

mysql userid/password can be found in the application.properties file inside the jar (BOOT-INF\classes\application.properties)
database / tables are automatically created on startup. The following privileges should be granted to the user:  
CREATE  
DROP  
INSERT  
SELECT  
SUPER
  


