/*Load access log into the database*/
LOAD DATA LOCAL INFILE :fileLocation
INTO TABLE accesslog
  FIELDS
    TERMINATED BY '|'
    ENCLOSED BY '\"'
  LINES
    TERMINATED BY '\\r\\n'
(DATE, ip, request, STATUS, useragent)

/*LOAD DATA INFILE requires the local_infile GLOBAL to be set to ON*/
SET GLOBAL local_infile = 'ON';

/*Select ip addresses based on the arguments*/
SELECT
  ip,
  COUNT(ip) AS cnt
FROM
  accesslog
WHERE DATE BETWEEN 'Sun Jan 01 13:00:00 GFT 2017'
  AND DATE_ADD('Sun Jan 01 13:00:00 GFT 2017',INTERVAL 1 DAY)
GROUP BY ip
HAVING cnt >= 200

/*Insert ip address and reason into the blockedip table*/
INSERT INTO blockedip (ip, reason)
VALUES('192.168.234.82', '200 or more request per day starting at 2017-01-01 00:00:00')

/*Select blocked ip address for display*/
SELECT id, reason
FROM blockedip
